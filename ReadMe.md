# Remote Execution Library (librexec) #

Librexec is an extension of Javas RMI implementation. It is a standalone system to transmit and decode objects over a network. A librexec app may consist of several classes which may include non-remote methods. In general all standard objects are encode/decodeable and therefore transmittable over TCP. For specific classes there is the possibility to define your own PackageEntityWriter/Reader so that the librexec system is able to transmit your special classes with non-standard data types.

## ToDo ##
- Implement Notification API
- Add Authenticator and according mechanisms
- Encode/decode special bytes such as NULL\_BYTE, ETX\_BYTE and EOT\_BYTE
- Implement DataStreamPackage
