/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lib.rexec.net;

import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;
import lib.rexec.entity.Packet;
import lib.rexec.io.reader.PacketReader;

/**
 *
 * @author thomas
 */
public class ClientHandler extends NetService {
    
    private final Socket socket;
    
    public ClientHandler(Socket socket){
        this.socket = socket;
    }
    
    public ClientHandler(Socket socket, Receiver listener){
        this(socket);
        listeners.add(listener);
    }

    @Override
    public void run() {
        try (InputStream inputStream = socket.getInputStream();
             PacketReader reader = new PacketReader(inputStream)){
            
            while (!socket.isClosed()){
                Packet packet = reader.read();
                receive(packet, socket);
            }
        } catch (Exception ex){
            error(ex);
        }
    }
    
    @Override
    public void close() throws IOException {
        socket.close();
    }
}
