/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lib.rexec.net;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import lib.rexec.entity.Packet;
import lib.rexec.io.writer.PacketWriter;

/**
 *
 * @author thomas
 */
public class Server extends NetService {
    
    private final List<ClientHandler> handlers;
    
    private final ServerSocket socket;
    
    public Server() throws IOException{
        this(DEFAULT_PORT);
    }
    
    public Server(int port) throws IOException{
        super(port);
        this.handlers = new ArrayList<>();
        this.socket = new ServerSocket();
    }
    
    public Server(int port, Receiver listener) throws IOException{
        super(port, listener);
        this.handlers = new ArrayList<>();
        this.socket = new ServerSocket();
    }
    
    public void send(Packet packet, Socket socket) throws IOException{
        PacketWriter writer = new PacketWriter(socket.getOutputStream());
        writer.write(packet);
    }

    @Override
    public void run() {
        try {
            socket.bind(new InetSocketAddress(port));
            
            while (!socket.isClosed()){
                Socket conn = socket.accept();
                ClientHandler handler = new ClientHandler(conn, this);
                
                handlers.add(handler);
                
                Thread thread = new Thread(handler);
                thread.start();
            }
        } catch (Exception ex){
            error(ex);
        }
    }
    
    @Override
    public void close() throws IOException {
        for (ClientHandler handler : handlers){
            handler.close();
        }
        socket.close();
    }
}
