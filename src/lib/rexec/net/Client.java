/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lib.rexec.net;

import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.Socket;
import lib.rexec.entity.Packet;
import lib.rexec.io.reader.PacketReader;
import lib.rexec.io.writer.PacketWriter;

/**
 *
 * @author thomas
 */
public class Client extends NetService {
    
    private final Socket socket;
    
    public Client(InetAddress address) throws IOException{
        this(address, DEFAULT_PORT);
    }
    
    public Client(InetAddress address, int port) throws IOException{
        super(port);
        socket = new Socket(address, port);
    }
    
    public void send(Packet packet) throws IOException{
        PacketWriter writer = new PacketWriter(socket.getOutputStream());
        writer.write(packet);
    }

    @Override
    public void run() {
        try (InputStream inputStream = socket.getInputStream();
             PacketReader reader = new PacketReader(inputStream)){
            
            while (!socket.isClosed()){
                Packet p = reader.read();
                receive(p, socket);
            }
        } catch (Exception ex){
            error(ex);
        }
    }
    
    @Override
    public void close() throws Exception {
        socket.close();
    }
}
