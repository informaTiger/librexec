/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lib.rexec.net;

import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import lib.rexec.entity.Packet;

/**
 *
 * @author thomas
 */
public abstract class NetService implements Runnable, Receiver, AutoCloseable {
    
    public static final int DEFAULT_PORT = 9128;
    
    protected final List<Receiver> listeners;
    
    protected int port;
    
    public NetService(){
        this(DEFAULT_PORT);
    }
    
    public NetService(int port){
        this.port = port;
        listeners = new ArrayList<>();
    }
    
    public NetService(int port, Receiver listener){
        this(port);
        listeners.add(listener);
    }

    public int getPort() {
        return port;
    }
    
    public void addListener(Receiver listener){
        listeners.add(listener);
    }
    
    public void removeListener(Receiver listener){
        listeners.remove(listener);
    }

    @Override
    public final void receive(Packet packet, Socket sender) {
        for (Receiver listener : listeners){
            listener.receive(packet, sender);
        }
    }
    
    @Override
    public final void error(Exception ex) {
        for (Receiver listener : listeners){
            listener.error(ex);
        }
    }
}