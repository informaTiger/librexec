/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lib.rexec.net;

import java.net.Socket;
import lib.rexec.entity.Packet;

/**
 *
 * @author thomas
 */
public interface Receiver {
    
    public void receive(Packet packet, Socket sender);
    
    public void error(Exception ex);
}
