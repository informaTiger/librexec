/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lib.rexec;

import java.io.IOException;
import java.net.Socket;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import lib.rexec.entity.CommandPacket;
import lib.rexec.entity.DataPacket;
import lib.rexec.entity.Packet;
import lib.rexec.entity.PacketType;
import lib.rexec.net.Receiver;
import lib.rexec.net.Server;

/**
 *
 * @author thomas
 */
public class RexecProvider implements Runnable, Receiver {
    
    private final HashMap<String, RexecApplication> registry;
    
    private final Server server;
    
    public RexecProvider() throws IOException{
        server = new Server();
        registry = new HashMap<>();
        
        server.addListener(this);
    }
    
    public RexecProvider(int port) throws IOException{
        server = new Server(port, this);
        registry = new HashMap<>();
    }
    
    public void register(RexecApplication app) {
        registry.put(app.getName(), app);
    }

    @Override
    public void run() {
        server.run();
    }

    @Override
    public void receive(Packet packet, Socket sender) {
        if (packet.getType() == PacketType.COMMAND){
            CommandPacket cmd = (CommandPacket)packet;
            
            try {
                if (!registry.containsKey(cmd.getEntity().getApp())){
                    throw new IllegalArgumentException(String.format(
                            "Application %s does not exist",
                            cmd.getEntity().getApp()
                        )
                    );
                }
                RexecApplication app = registry.get(cmd.getEntity().getApp());                
                Object obj = app.invoke(cmd.getEntity(), cmd.getParameters());                
                
                server.send(new DataPacket.Builder()
                    .target(obj.getClass())
                    .data(obj)
                    .build(),
                    sender
                );
            } catch (IllegalArgumentException | ReflectiveOperationException | IOException ex){
                handleException(ex, sender);
            }
        }
    }
    
    @Override
    public void error(Exception ex) {
        Logger.getLogger(RexecProvider.class.getName()).log(Level.SEVERE, ex.getMessage());
    }
    
    private void handleException(Exception ex, Socket sender){        
        try {
            server.send(new DataPacket.Builder()
                .target(RemoteException.class)
                .data(new RemoteException(ex))
                .build(),
                sender
            );
        } catch (IOException ioe){ /* can't do anything here */ }
    }
}