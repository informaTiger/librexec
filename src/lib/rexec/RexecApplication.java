/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lib.rexec;

import java.lang.reflect.Method;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import lib.rexec.entity.Entity;

/**
 *
 * @author Thomas
 */
public class RexecApplication {
    
    private final Set<Method> registry;
    
    private final String name;
    
    public RexecApplication(String name){
        if (name == null){
            throw new NullPointerException();
        }
        this.name = name;
        registry = new HashSet<>();
    }
    
    public void register(Class clazz){
        for (Method m : clazz.getMethods()){
            if (m.isAnnotationPresent(RexecMethod.class)){
                registry.add(m);
            }
        }
    }

    public String getName() {
        return name;
    }
    
    public Object invoke(Entity entity, Object... args) throws ReflectiveOperationException {
        Method method = entity.resolveMethod(args);
        
        if (!registry.contains(method)){
            throw new IllegalArgumentException(String.format(
                    "It's not allowed to invoke %s",
                    method.getName()
                )
            );
        }
        Class type = method.getDeclaringClass();
        return method.invoke(type.newInstance(), args);
    }
    
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 29 * hash + Objects.hashCode(this.name);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final RexecApplication other = (RexecApplication) obj;
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        return true;
    }
}