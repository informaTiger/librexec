/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lib.rexec;

import java.lang.reflect.InvocationTargetException;

/**
 *
 * @author Thomas
 */
public class RemoteException extends Exception {

    public RemoteException() {
        super();
    }
    
    public RemoteException(String message){
        super(message);
    }
    
    public RemoteException(Throwable ex){
        super(ex.getMessage(), ex);
    }
    
    public RemoteException(InvocationTargetException ex){
        super(ex.getTargetException().getMessage(), ex.getTargetException());
    }
}
