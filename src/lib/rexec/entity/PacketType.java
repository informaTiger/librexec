/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lib.rexec.entity;

/**
 *
 * @author Thomas
 */
public enum PacketType {
    COMMAND(1), DATA(2), STREAM(3);

    private final int id;
    
    private PacketType(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }
    
    public static PacketType getById(int id){
        for (PacketType type : PacketType.values()){
            if (type.getId() == id){
                return type;
            }
        }
        throw new IllegalArgumentException(String.format(
                "No such PacketType: %s",
                id
            )
        );
    }
}
