/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lib.rexec.entity;

import java.lang.reflect.Method;
import java.util.Objects;

/**
 *
 * @author Thomas
 */
public class Entity {
    
    public static final String DELIMITER = "::";
    
    private final String app;
    
    private final String target;
    
    private final String method;
    
    private Entity(Builder builder){
        this.app = builder.app;
        this.target = builder.target;
        this.method = builder.method;
    }
    
    public static class Builder {
        
        private String app;
        
        private String target;
        
        private String method;
        
        public Builder app(String app){
            this.app = app;
            return this;
        }
        
        public Builder target(String target){
            this.target = target;
            return this;
        }
        
        public Builder target(Class target){
            this.target = target.getName();
            return this;
        }
        
        public Builder method(String method){
            this.method = method;
            return this;
        }
        
        public Builder method(Method method){
            this.method = method.getName();
            return this;
        }
        
        public Entity build(){
            return new Entity(this);
        }
    }

    public String getApp() {
        return app;
    }

    public String getTarget() {
        return target;
    }

    public String getMethod() {
        return method;
    }
    
    public Class resolveTarget() throws ReflectiveOperationException{
        return Class.forName(target);
    }
    
    public Method resolveMethod(Object... args) throws ReflectiveOperationException{
        Class type = resolveTarget();
        return type.getDeclaredMethod(method, getParameterTypes(args));
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 89 * hash + Objects.hashCode(this.app);
        hash = 89 * hash + Objects.hashCode(this.target);
        hash = 89 * hash + Objects.hashCode(this.method);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Entity other = (Entity) obj;
        if (!Objects.equals(this.app, other.app)) {
            return false;
        }
        if (!Objects.equals(this.target, other.target)) {
            return false;
        }
        if (!Objects.equals(this.method, other.method)) {
            return false;
        }
        return true;
    }
    
    @Override
    public String toString(){
        StringBuilder res = new StringBuilder();
        
        res.append(app).append(DELIMITER);
        res.append(target).append(DELIMITER);
        res.append(method);
        
        return res.toString();
    }
    
    public static Entity forInput(String value){
        String[] path = value.split(DELIMITER);
        
        if (path.length < 3){
            throw new IllegalArgumentException("Illegal entity string.");
        }
        return new Entity.Builder()
            .app(path[0])
            .target(path[1])
            .method(path[2])
            .build();
    }
    
    private static Class[] getParameterTypes(Object... args){
        Class[] classes = new Class[args.length];
        
        for (int i = 0; i < args.length; i++){
            classes[i] = args[i].getClass();
        }
        return classes;
    }
}
