/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lib.rexec.entity;

import java.util.Arrays;
import java.util.Objects;

/**
 *
 * @author Thomas
 */
public class CommandPacket extends Packet {
    
    private Entity entity;
    
    private Object[] parameters;
    
    public CommandPacket(){
        super(PacketType.COMMAND);
        parameters = new Object[0];
    }
    
    private CommandPacket(Builder builder){
        super(PacketType.COMMAND);
        
        this.entity = builder.entity;
        this.parameters = builder.parameters;
        
        if (parameters == null){
            parameters = new Object[0];
        }
    }
    
    public static class Builder {
        
        private Entity entity;
        
        private Object[] parameters;
        
        public Builder entity(String entity){
            this.entity = Entity.forInput(entity);
            return this;
        }
        
        public Builder entity(Entity entity){
            this.entity = entity;
            return this;
        }
        
        public Builder params(Object... params){
            this.parameters = params;
            return this;
        }
        
        public CommandPacket build(){
            return new CommandPacket(this);
        }
    }

    public Entity getEntity() {
        return entity;
    }

    public void setEntity(Entity entity) {
        this.entity = entity;
    }
    
    public Object[] getParameters() {
        return parameters;
    }

    public void setParameters(Object[] parameters) {
        if (parameters == null){
            throw new NullPointerException();
        }
        this.parameters = parameters;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final CommandPacket other = (CommandPacket) obj;
        if (!Objects.equals(this.entity, other.entity)) {
            return false;
        }
        if (!Arrays.deepEquals(this.parameters, other.parameters)) {
            return false;
        }
        return true;
    }
}
