/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lib.rexec.entity;

/**
 *
 * @author Thomas
 */
public class DataStreamPacket extends DataPacket<byte[]> {
    
    private int sequenceId;
    
    private int total;
    
    public DataStreamPacket(){
        super(byte[].class);
        
        this.type = PacketType.STREAM; // override PacketType.DATA given by parent class
        this.total = 1;
    }
    
    private DataStreamPacket(Builder builder){
        super(byte[].class);
        
        this.type = PacketType.STREAM;
        this.data = builder.data;        
        this.total = builder.total;
        this.sequenceId = builder.sequenceId;
        
        checkPacketMeta();
    }
    
    public static class Builder {
        
        private byte[] data;
        
        private int sequenceId = 0;
        
        private int total = 1;
        
        public Builder data(byte[] data){
            this.data = data;
            return this;
        }
        
        public Builder sequenceId(int sequenceId){
            this.sequenceId = sequenceId;
            return this;
        }
        
        public Builder total(int total){
            this.total = total;
            return this;
        }
        
        public DataStreamPacket build(){
            return new DataStreamPacket(this);
        }
    }

    public int getSequenceId() {
        return sequenceId;
    }

    public void setSequenceId(int sequenceId) {
        this.sequenceId = sequenceId;
        checkPacketMeta();
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
        checkPacketMeta();
    }
    
    private void checkPacketMeta(){
        if (sequenceId < 0 || sequenceId > total){ // tests that total is not negative as well
            throw new IllegalArgumentException("Sequence id can not be negative and must be smaller or equal than total packet count");
        }
    }

    @Override
    public int hashCode() {
        int hash = 5;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final DataStreamPacket other = (DataStreamPacket) obj;
        if (this.sequenceId != other.sequenceId) {
            return false;
        }
        return this.total == other.total;
    }
}
