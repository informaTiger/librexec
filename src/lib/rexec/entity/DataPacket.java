/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lib.rexec.entity;

import java.util.Objects;

/**
 *
 * @author Thomas
 * @param <T>
 */
public class DataPacket<T> extends Packet {
    
    protected final Class<T> target;
    
    protected T data;
    
    public DataPacket(Class<T> target){
        super(PacketType.DATA);
        this.target = target;
    }
    
    private DataPacket(Builder<T> builder){
        super(PacketType.DATA);
        
        this.target = builder.target;
        this.data = builder.data;
    }
    
    public static class Builder<T> {
        
        private Class<T> target;
        
        private T data;
        
        public Builder target(Class<T> target){
            this.target = target;
            return this;
        }
        
        public Builder data(T data){
            this.data = data;
            return this;
        }
        
        public DataPacket<T> build(){
            return new DataPacket<>(this);
        }
    }
    
    public Class<T> getTarget() {
        return target;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 31 * hash + Objects.hashCode(this.target);
        hash = 31 * hash + Objects.hashCode(this.data);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final DataPacket<?> other = (DataPacket<?>) obj;
        if (!Objects.equals(this.target, other.target)) {
            return false;
        }
        if (!Objects.equals(this.data, other.data)) {
            return false;
        }
        return true;
    }    
}
