/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lib.rexec.entity;

/**
 *
 * @author Thomas
 */
public abstract class Packet {
    
    protected PacketType type;
    
    public Packet(){
        
    }
    
    public Packet(PacketType type){
        this.type = type;
    }

    public PacketType getType() {
        return type;
    }
}
