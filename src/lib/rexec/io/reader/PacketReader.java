/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lib.rexec.io.reader;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import lib.rexec.entity.CommandPacket;
import lib.rexec.entity.DataPacket;
import lib.rexec.entity.Packet;
import lib.rexec.entity.PacketType;
import lib.io.ByteBuffer;
import lib.rexec.entity.DataStreamPacket;
import lib.rexec.entity.Entity;

/**
 *
 * @author thomas
 */
public class PacketReader implements AutoCloseable {
    
    public static final int NULL_BYTE = 0x00;
    
    public static final int EOT_BYTE = 0x04;
    
    private final InputStream inputStream;
    
    private boolean eot = false;
    
    public PacketReader(InputStream inputStream){
        this.inputStream = inputStream;
    }
    
    public Packet read() throws IOException, ClassNotFoundException{
        int id = Integer.valueOf(new String(readUntil(NULL_BYTE)));
        PacketType type = PacketType.getById(id);
        
        Packet packet;
        if (type == PacketType.COMMAND){
            packet = new CommandPacket();
            readCommandPacket((CommandPacket)packet);
        } else if (type == PacketType.DATA) {
            Class target = Class.forName(new String(readUntil(NULL_BYTE)));
            packet = new DataPacket(target);
            
            readDataPacket((DataPacket)packet);
        } else {
            packet = new DataStreamPacket();
            readDataStreamPacket((DataStreamPacket)packet);
        }
        return packet;
    }
    
    private void readCommandPacket(CommandPacket packet) throws IOException, ClassNotFoundException{
        packet.setEntity(Entity.forInput(new String(readUntil(NULL_BYTE))));
        
        String cur;
        List params = new ArrayList();
        while (!eot && !(cur = new String(readUntil(NULL_BYTE))).equals("")){
            Class type = Class.forName(cur);
            Object obj = readEntity(type);
            
            params.add(obj);
        }
        packet.setParameters(params.toArray(new Object[params.size()]));
        
        eot = false;
    }
    
    private void readDataPacket(DataPacket packet) throws IOException{
        Object obj = readEntity(packet.getTarget());        
        packet.setData(obj);
    }
    
    private void readDataStreamPacket(DataStreamPacket packet) throws IOException{
        packet.setSequenceId(Integer.parseInt(new String(readUntil(NULL_BYTE))));
        packet.setTotal(Integer.parseInt(new String(readUntil(NULL_BYTE))));
        
        readDataPacket(packet);
    }
    
    private Object readEntity(Class type) throws IOException{
        PacketEntityReader reader = PacketEntityReader.Registry.get(type);
        
        if (reader instanceof AbstractEntityReader){
            return ((AbstractEntityReader)reader).read(type, readUntil(NULL_BYTE));
        }
        return reader.read(readUntil(NULL_BYTE));
    }
    
    private byte[] readUntil(int delimiter) throws IOException{
        int data;
        ByteBuffer buffer = new ByteBuffer();
        
        while ((data = inputStream.read()) != delimiter && data != EOT_BYTE){
            buffer.put(data);
        }
        
        if (data == EOT_BYTE){
            eot = true;
        }
        return buffer.toArray();
    }

    @Override
    public void close() throws IOException {
        inputStream.close();
    }
}
