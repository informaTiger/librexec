/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lib.rexec.io.reader;

import java.io.IOException;
import lib.io.ByteBuffer;

/**
 *
 * @author thomas
 */
public abstract class AbstractEntityReader<T> implements PacketEntityReader<T> {
    
    public static final int ETX_BYTE = 0x03;
    
    private int pos;
    
    public AbstractEntityReader(){
        
    }
    
    public abstract T read(Class<T> type, byte[] input) throws IOException;
    
    protected byte[] readUntil(byte[] input, int delimiter){
        ByteBuffer buf = new ByteBuffer();
        
        for (int i = pos; i < input.length; i++){
            if (input[i] == delimiter){
                pos = i + 1;
                break;
            }
            buf.put(input[i]);
        }
        return buf.toArray();
    }
}
