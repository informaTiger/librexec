/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lib.rexec.io.reader.defaults;

import java.io.IOException;
import lib.rexec.io.reader.PacketEntityReader;

/**
 *
 * @author thomas
 */
public class ShortEntityReader implements PacketEntityReader<Short> {

    @Override
    public Short read(byte[] input) throws IOException {
        return Short.parseShort(new String(input));
    }
}
