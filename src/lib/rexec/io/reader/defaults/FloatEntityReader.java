/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lib.rexec.io.reader.defaults;

import java.io.IOException;
import lib.rexec.io.reader.PacketEntityReader;

/**
 *
 * @author thomas
 */
public class FloatEntityReader implements PacketEntityReader<Float> {

    @Override
    public Float read(byte[] input) throws IOException {
        return Float.parseFloat(new String(input));
    }
}
