/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lib.rexec.io.reader.defaults;

import java.io.IOException;
import java.lang.reflect.Field;
import lib.rexec.io.reader.AbstractEntityReader;
import lib.rexec.io.reader.PacketEntityReader;

/**
 *
 * @author thomas
 */
public class ObjectEntityReader extends AbstractEntityReader<Object> {

    @Override
    public Object read(byte[] input) throws IOException { // just for backward compatibility
        try {
            Class type = Class.forName(new String(readUntil(input, ETX_BYTE)));
            return read(type, input);
        } catch (ClassNotFoundException ex){
            throw new IOException(ex);
        }
    }

    @Override
    public Object read(Class<Object> type, byte[] input) throws IOException {
        try {
            int i = 0;
            byte[] buf;
            Object obj = type.newInstance();
            Field[] fields = type.getDeclaredFields();
            
            while ((buf = readUntil(input, ETX_BYTE)).length > 0){
                fields[i].setAccessible(true);
                
                PacketEntityReader reader = PacketEntityReader.Registry.get(fields[i].getType());
                fields[i++].set(obj, reader.read(buf));
            }
            return obj;
        } catch (InstantiationException | IllegalAccessException | SecurityException ex){
            throw new IOException(ex);
        }
    }
}
