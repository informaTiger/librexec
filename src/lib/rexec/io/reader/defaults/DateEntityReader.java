/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lib.rexec.io.reader.defaults;

import java.io.IOException;
import java.util.Date;
import lib.rexec.io.reader.PacketEntityReader;

/**
 *
 * @author Thomas
 */
public class DateEntityReader implements PacketEntityReader<Date> {

    @Override
    public Date read(byte[] input) throws IOException {
        return new Date(Long.parseLong(new String(input), 16));
    }
}
