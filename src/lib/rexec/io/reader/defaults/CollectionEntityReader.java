/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lib.rexec.io.reader.defaults;

import java.io.IOException;
import java.util.AbstractCollection;
import lib.rexec.io.reader.AbstractEntityReader;
import lib.rexec.io.reader.PacketEntityReader;

/**
 *
 * @author Thomas
 */
public class CollectionEntityReader extends AbstractEntityReader<AbstractCollection> {
    
    @Override
    public AbstractCollection read(byte[] input) throws IOException { // just for backward compatibility
        try {
            Class<AbstractCollection> type = (Class<AbstractCollection>)Class.forName(new String(readUntil(input, ETX_BYTE)));
            return read(type, input);
        } catch (ClassNotFoundException ex){
            throw new IOException(ex);
        }
    }

    @Override
    public AbstractCollection read(Class<AbstractCollection> type, byte[] input) throws IOException {
        try {
            Class subtype = Class.forName(new String(readUntil(input, ETX_BYTE)));
            
            AbstractCollection res = type.newInstance();
            byte[] buf;
            while ((buf = readUntil(input, ETX_BYTE)).length > 0){                
                PacketEntityReader reader = PacketEntityReader.Registry.get(subtype);
                res.add(reader.read(buf));
            }
            return res;
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException ex){
            throw new IOException(ex);
        }
    }
}
