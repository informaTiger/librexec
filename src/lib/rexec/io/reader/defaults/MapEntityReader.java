/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lib.rexec.io.reader.defaults;

import java.io.IOException;
import java.util.AbstractMap;
import lib.rexec.io.reader.AbstractEntityReader;
import lib.rexec.io.reader.PacketEntityReader;

/**
 *
 * @author thomas
 */
public class MapEntityReader extends AbstractEntityReader<AbstractMap> {
    
    @Override
    public AbstractMap read(byte[] input) throws IOException { // just for backward compatibility
        try {
            Class<AbstractMap> type = (Class<AbstractMap>)Class.forName(new String(readUntil(input, ETX_BYTE)));
            return read(type, input);
        } catch (ClassNotFoundException ex){
            throw new IOException(ex);
        }
    }

    @Override
    public AbstractMap read(Class<AbstractMap> type, byte[] input) throws IOException {
        try {
            Class keyType = Class.forName(new String(readUntil(input, ETX_BYTE)));
            Class valueType = Class.forName(new String(readUntil(input, ETX_BYTE)));
            
            AbstractMap res = type.newInstance();            
            byte[] buf;
            Object nextKey = null;
            boolean readKey = true;
            
            while ((buf = readUntil(input, ETX_BYTE)).length > 0){
                PacketEntityReader reader = PacketEntityReader.Registry.get(readKey ? keyType : valueType);
                if (readKey){
                    nextKey = reader.read(buf);
                } else {
                    res.put(nextKey, reader.read(buf));
                }
                readKey = !readKey;
            }
            return res;
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException ex){
            throw new IOException(ex);
        }
    }
}
