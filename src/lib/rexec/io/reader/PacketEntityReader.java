/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lib.rexec.io.reader;

import java.io.IOException;
import java.util.AbstractCollection;
import java.util.AbstractMap;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import lib.rexec.RemoteException;
import lib.rexec.io.reader.defaults.BooleanEntityReader;
import lib.rexec.io.reader.defaults.ByteEntityReader;
import lib.rexec.io.reader.defaults.ByteStreamEntityReader;
import lib.rexec.io.reader.defaults.CharacterEntityReader;
import lib.rexec.io.reader.defaults.CollectionEntityReader;
import lib.rexec.io.reader.defaults.DateEntityReader;
import lib.rexec.io.reader.defaults.DoubleEntityReader;
import lib.rexec.io.reader.defaults.RemoteExceptionEntityReader;
import lib.rexec.io.reader.defaults.FloatEntityReader;
import lib.rexec.io.reader.defaults.IntegerEntityReader;
import lib.rexec.io.reader.defaults.MapEntityReader;
import lib.rexec.io.reader.defaults.ObjectEntityReader;
import lib.rexec.io.reader.defaults.ShortEntityReader;
import lib.rexec.io.reader.defaults.StringEntityReader;

/**
 *
 * @author Thomas
 */
public interface PacketEntityReader<T> {
    
    public static class Registry {
        
        private static final Map<Class, PacketEntityReader> REGISTRY;
        
        static {
            REGISTRY = new HashMap<>();
            initDefaults();            
        }
        
        private static void initDefaults(){
            register(Boolean.class, new BooleanEntityReader());
            register(Byte.class, new ByteEntityReader());
            register(byte[].class, new ByteStreamEntityReader());
            register(Character.class, new CharacterEntityReader());
            register(AbstractCollection.class, new CollectionEntityReader());
            register(Date.class, new DateEntityReader());
            register(Double.class, new DoubleEntityReader());
            register(Float.class, new FloatEntityReader());
            register(Integer.class, new IntegerEntityReader());
            register(AbstractMap.class, new MapEntityReader());
            register(Object.class, new ObjectEntityReader());
            register(RemoteException.class, new RemoteExceptionEntityReader());
            register(Short.class, new ShortEntityReader());
            register(String.class, new StringEntityReader());
        }
        
        public static <T> void register(Class<T> type, PacketEntityReader<T> reader){
            if (type == null || reader == null){
                throw new NullPointerException();
            }
            REGISTRY.put(type, reader);
        }

        public static <T> PacketEntityReader<T> get(Class<T> type){
            if (type == null){
                throw new NullPointerException();
            }

            PacketEntityReader<T> reader = get0(type);
            if (reader == null){             
                throw new IllegalArgumentException(String.format(
                        "No reader registered for class %s",
                        type.getName()
                    )
                );
            }
            return reader;
        }
        
        private static <T> PacketEntityReader<T> get0(Class<T> type){
            PacketEntityReader<T> reader = REGISTRY.get(type);
            
            if (reader == null){
                Class sup = type.getSuperclass(); // check for existing super classes as well
                
                if (sup != null){
                    return get0(sup);
                }
            }
            return reader;
        }
    }
    
    public T read(byte[] input) throws IOException;
}
