/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lib.rexec.io.writer;

import java.io.IOException;
import java.io.OutputStream;
import java.util.AbstractCollection;
import java.util.AbstractMap;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import lib.rexec.RemoteException;
import lib.rexec.io.writer.defaults.BooleanEntityWriter;
import lib.rexec.io.writer.defaults.ByteEntityWriter;
import lib.rexec.io.writer.defaults.ByteStreamEntityWriter;
import lib.rexec.io.writer.defaults.CharacterEntityWriter;
import lib.rexec.io.writer.defaults.CollectionEntityWriter;
import lib.rexec.io.writer.defaults.DateEntityWriter;
import lib.rexec.io.writer.defaults.DoubleEntityWriter;
import lib.rexec.io.writer.defaults.RemoteExceptionEntityWriter;
import lib.rexec.io.writer.defaults.FloatEntityWriter;
import lib.rexec.io.writer.defaults.IntegerEntityWriter;
import lib.rexec.io.writer.defaults.MapEntityWriter;
import lib.rexec.io.writer.defaults.ObjectEntityWriter;
import lib.rexec.io.writer.defaults.ShortEntityWriter;
import lib.rexec.io.writer.defaults.StringEntityWriter;

/**
 *
 * @author Thomas
 */
public interface PacketEntityWriter<T> {
    
    public static class Registry {
     
        private static final Map<Class, PacketEntityWriter> REGISTRY;
        
        static {
            REGISTRY = new HashMap<>();
            initDefaults();
        }
        
        private static void initDefaults(){
            register(Boolean.class, new BooleanEntityWriter());
            register(Byte.class, new ByteEntityWriter());
            register(byte[].class, new ByteStreamEntityWriter());
            register(Character.class, new CharacterEntityWriter());
            register(AbstractCollection.class, new CollectionEntityWriter());
            register(Date.class, new DateEntityWriter());
            register(Double.class, new DoubleEntityWriter());
            register(Float.class, new FloatEntityWriter());
            register(Integer.class, new IntegerEntityWriter());
            register(AbstractMap.class, new MapEntityWriter());
            register(Object.class, new ObjectEntityWriter());
            register(RemoteException.class, new RemoteExceptionEntityWriter());
            register(Short.class, new ShortEntityWriter());
            register(String.class, new StringEntityWriter());
        }
        
        public static <T> void register(Class<T> type, PacketEntityWriter<T> writer){
            if (type == null || writer == null){
                throw new NullPointerException();
            }
            REGISTRY.put(type, writer);
        }

        public static <T> PacketEntityWriter<T> get(Class<T> type){
            if (type == null){
                throw new NullPointerException();
            }

            PacketEntityWriter<T> writer = get0(type);
            if (writer == null){
                throw new IllegalArgumentException(String.format(
                        "No writer registered for class %s",
                        type.getName()
                    )
                );
            }
            return writer;
        }
        
        private static <T> PacketEntityWriter<T> get0(Class<T> type){
            PacketEntityWriter<T> writer = REGISTRY.get(type);
            
            if (writer == null){
                Class sup = type.getSuperclass(); // check for existing super classes as well
                
                if (sup != null){
                    return get0(sup);
                }
            }            
            return writer;
        }
    }    
    
    public void write(T obj, OutputStream outputStream) throws IOException;
}
