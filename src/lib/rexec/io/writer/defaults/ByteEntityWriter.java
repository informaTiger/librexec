/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lib.rexec.io.writer.defaults;

import java.io.IOException;
import java.io.OutputStream;
import lib.rexec.io.writer.PacketEntityWriter;

/**
 *
 * @author thomas
 */
public class ByteEntityWriter implements PacketEntityWriter<Byte> {

    @Override
    public void write(Byte obj, OutputStream outputStream) throws IOException {
        outputStream.write(obj);
    }
}