/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lib.rexec.io.writer.defaults;

import java.io.IOException;
import java.io.OutputStream;
import lib.rexec.RemoteException;
import lib.rexec.io.writer.PacketEntityWriter;

/**
 *
 * @author Thomas
 */
public class RemoteExceptionEntityWriter implements PacketEntityWriter<RemoteException> {

    @Override
    public void write(RemoteException obj, OutputStream outputStream) throws IOException {
        outputStream.write(obj.getMessage() == null ? "".getBytes() : obj.getMessage().getBytes());
    }
}
