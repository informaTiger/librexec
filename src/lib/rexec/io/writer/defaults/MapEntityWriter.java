/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lib.rexec.io.writer.defaults;

import java.io.IOException;
import java.io.OutputStream;
import java.util.AbstractMap;
import java.util.Map;
import java.util.Set;
import lib.rexec.io.writer.AbstractEntityWriter;
import lib.rexec.io.writer.PacketEntityWriter;

/**
 *
 * @author thomas
 */
public class MapEntityWriter extends AbstractEntityWriter<AbstractMap> {

    @Override
    public void write(AbstractMap obj, OutputStream outputStream) throws IOException { // just for backward compatibility
        write(obj, outputStream, true);
    }

    @Override
    public void write(AbstractMap obj, OutputStream outputStream, boolean includeType) throws IOException {
        if (includeType){
            write(obj.getClass().getName().getBytes(), outputStream);
        }
        write(getChildClass(obj.keySet()).getName().getBytes(), outputStream);
        write(getChildClass(obj.values()).getName().getBytes(), outputStream);
        
        for (Map.Entry e : (Set<Map.Entry>)obj.entrySet()){
            PacketEntityWriter writer = PacketEntityWriter.Registry.get(e.getKey().getClass());
            writer.write(e.getKey(), outputStream);
            
            outputStream.write(ETX_BYTE);
            
            writer = PacketEntityWriter.Registry.get(e.getValue().getClass());
            writer.write(e.getValue(), outputStream);
            outputStream.write(ETX_BYTE);
        }
    }
}
