/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lib.rexec.io.writer.defaults;

import java.io.IOException;
import java.io.OutputStream;
import java.util.AbstractCollection;
import lib.rexec.io.writer.AbstractEntityWriter;
import lib.rexec.io.writer.PacketEntityWriter;

/**
 *
 * @author Thomas
 */
public class CollectionEntityWriter extends AbstractEntityWriter<AbstractCollection> {
    
    @Override
    public void write(AbstractCollection obj, OutputStream outputStream) throws IOException { // just for backward compatibility
        write(obj, outputStream, true);
    }

    @Override
    public void write(AbstractCollection obj, OutputStream outputStream, boolean includeType) throws IOException {
        if (includeType){
            write(obj.getClass().getName().getBytes(), outputStream);
        }
        write(getChildClass(obj).getName().getBytes(), outputStream);
        
        for (Object o : obj){
            PacketEntityWriter writer = PacketEntityWriter.Registry.get(o.getClass());
            writer.write(o, outputStream);
            
            outputStream.write(ETX_BYTE);
        }
    }
}
