/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lib.rexec.io.writer.defaults;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Date;
import lib.rexec.io.writer.PacketEntityWriter;

/**
 *
 * @author Thomas
 */
public class DateEntityWriter implements PacketEntityWriter<Date> {

    @Override
    public void write(Date obj, OutputStream outputStream) throws IOException {
        outputStream.write(Long.toHexString(obj.getTime()).getBytes());
    }
}
