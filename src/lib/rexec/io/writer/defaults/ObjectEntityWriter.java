/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lib.rexec.io.writer.defaults;

import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.Field;
import lib.rexec.io.writer.AbstractEntityWriter;
import lib.rexec.io.writer.PacketEntityWriter;

/**
 *
 * @author thomas
 */
public class ObjectEntityWriter extends AbstractEntityWriter<Object> {

    @Override
    public void write(Object obj, OutputStream outputStream) throws IOException { // just for backward compatibility
        write(obj, outputStream, true);
    }

    @Override
    public void write(Object obj, OutputStream outputStream, boolean includeType) throws IOException {
        if (includeType){
            write(obj.getClass().getName().getBytes(), outputStream);
        }
        
        try {
            for (Field field : obj.getClass().getDeclaredFields()){
                field.setAccessible(true);

                PacketEntityWriter writer = PacketEntityWriter.Registry.get(field.getType());
                writer.write(field.get(obj), outputStream);
                
                outputStream.write(ETX_BYTE);
            }
        } catch (SecurityException | IllegalArgumentException | IllegalAccessException ex){
            throw new IOException(ex);
        }
    }
}
