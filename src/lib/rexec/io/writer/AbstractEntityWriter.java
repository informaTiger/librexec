/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lib.rexec.io.writer;

import java.io.IOException;
import java.io.OutputStream;

/**
 *
 * @author thomas
 */
public abstract class AbstractEntityWriter<T> implements PacketEntityWriter<T> {
    
    public static final int ETX_BYTE = 0x03;
    
    public AbstractEntityWriter(){
        
    }
    
    public abstract void write(T obj, OutputStream outputStream, boolean includeType) throws IOException;
    
    protected void write(byte[] buf, OutputStream outputStream) throws IOException{
        outputStream.write(buf);
        outputStream.write(ETX_BYTE);
    }
    
    protected boolean ensureNext(Iterable iter){
        return iter.iterator().hasNext();
    }
    
    protected Class getChildClass(Iterable iter){
        if (ensureNext(iter)){
            return iter.iterator().next().getClass();
        }
        throw new IllegalStateException("Iterator has no elements");
    }
}
