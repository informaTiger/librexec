/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lib.rexec.io.writer;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import lib.rexec.entity.CommandPacket;
import lib.rexec.entity.DataPacket;
import lib.rexec.entity.DataStreamPacket;
import lib.rexec.entity.Packet;
import lib.rexec.entity.PacketType;

/**
 *
 * @author thomas
 */
public class PacketWriter implements AutoCloseable {
    
    public static final int NULL_BYTE = 0x00;
    
    public static final int EOT_BYTE = 0x04;
    
    private final OutputStream outputStream;
    
    public PacketWriter(OutputStream outputStream){
        this.outputStream = new BufferedOutputStream(outputStream); // that package data is buffered until it is fully constructed
    }
    
    public void write(Packet packet) throws IOException{
        if (packet.getType() == PacketType.COMMAND){
            write((CommandPacket)packet);
        } else if (packet.getType() == PacketType.DATA) {
            write((DataPacket)packet);
        } else {
            write((DataStreamPacket)packet);
        }
        outputStream.write(EOT_BYTE);
        
        outputStream.flush();
    }
    
    private void write(CommandPacket packet) throws IOException{
        writeHeader(packet);
        
        outputStream.write(packet.getEntity().toString().getBytes());
        outputStream.write(NULL_BYTE);
        
        int i = 0;
        for (Object obj : packet.getParameters()){
            outputStream.write(obj.getClass().getName().getBytes());
            outputStream.write(NULL_BYTE);
            
            writeEntity(obj.getClass(), obj);
            
            if (i < packet.getParameters().length - 1){
                outputStream.write(NULL_BYTE);
            }
            i++;
        }
    }
    
    private void write(DataPacket packet) throws IOException{  
        writeHeader(packet);
        
        outputStream.write(packet.getTarget().getName().getBytes());
        outputStream.write(NULL_BYTE);
        
        writeEntity(packet.getTarget(), packet.getData());
    }
    
    private void write(DataStreamPacket packet) throws IOException{
        writeHeader(packet);
        
        outputStream.write(String.valueOf(packet.getSequenceId()).getBytes());
        outputStream.write(NULL_BYTE);
        
        outputStream.write(String.valueOf(packet.getTotal()).getBytes());
        outputStream.write(NULL_BYTE);
        
        writeEntity(packet.getTarget(), packet.getData());
    }
    
    private void writeHeader(Packet packet) throws IOException{
        outputStream.write(String.valueOf(packet.getType().getId()).getBytes());
        outputStream.write(NULL_BYTE);
    }
    
    private void writeEntity(Class type, Object obj) throws IOException{
        PacketEntityWriter writer = PacketEntityWriter.Registry.get(type);
        
        if (writer instanceof AbstractEntityWriter){
            ((AbstractEntityWriter)writer).write(obj, outputStream, false);
            return;
        }
        writer.write(obj, outputStream);
    }

    @Override
    public void close() throws IOException {
        outputStream.close();
    }
}
