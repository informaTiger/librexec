/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lib.rexec;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import lib.rexec.entity.CommandPacket;
import lib.rexec.entity.DataPacket;
import lib.rexec.entity.DataStreamPacket;
import lib.rexec.entity.Packet;
import lib.rexec.io.reader.PacketReader;
import lib.rexec.io.writer.PacketWriter;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Thomas
 */
public class PacketTest {
    
    public static final int TEST_BSIZE = 32;
    
    public PacketTest() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void dataPacket() throws IOException, ClassNotFoundException{
        DataPacket<Date> datepkg = new DataPacket.Builder<Date>()
            .target(Date.class)
            .data(new Date())
            .build();
        
        DataPacket ret = (DataPacket)testPacket(datepkg);
        assertEquals(ret, datepkg);
    }
    
    @Test
    public void dataCollection() throws IOException, ClassNotFoundException{
        List<Integer> list = new ArrayList<>();
        list.add(3);
        list.add(5);
        
        DataPacket<ArrayList> colpkg = new DataPacket.Builder<ArrayList>()
            .target(ArrayList.class)
            .data(list)
            .build();
        
        DataPacket ret = (DataPacket)testPacket(colpkg);
        assertEquals(ret, colpkg);
    }
    
    @Test
    public void dataMap() throws IOException, ClassNotFoundException{
        Map<String, Integer> map = new HashMap<>();
        map.put("a", 17);
        map.put("b", 3);
        map.put("c", 8);
        
        DataPacket<HashMap> mappkg = new DataPacket.Builder<HashMap>()
            .target(HashMap.class)
            .data(map)
            .build();
        
        DataPacket ret = (DataPacket)testPacket(mappkg);
        assertEquals(ret, mappkg);
    }
    
    @Test
    public void dataObject() throws IOException, ClassNotFoundException, ParseException{
        Person me = new Person();
        me.setName("Thomas");
        me.setSurname("Thaler");
        me.setAge(21);
        me.setBirthday(new SimpleDateFormat("yyyy-M-dd").parse("1996-11-17"));
        
        DataPacket<Person> objpkg = new DataPacket.Builder<Person>()
            .target(Person.class)
            .data(me)
            .build();
        
        DataPacket ret = (DataPacket)testPacket(objpkg);
        assertEquals(ret, objpkg);
    }
    
    @Test
    public void dataStreamPacket() throws IOException, ClassNotFoundException{
        byte[] buffer = new byte[TEST_BSIZE];
        Random random = new Random();
        
        random.nextBytes(buffer);
        
        DataStreamPacket streampkg = new DataStreamPacket.Builder()
            .data(buffer)
            .sequenceId(1)
            .total(2)
            .build();
        
        DataStreamPacket ret = (DataStreamPacket)testPacket(streampkg);
        assertEquals(ret, streampkg);
    }
    
    @Test
    public void commandPacket() throws IOException, ClassNotFoundException{
        CommandPacket cmdpkg = new CommandPacket.Builder()
            .entity("Math::lib.rexec.MathResource::add")
            .params(3, 5)
            .build();
        
        CommandPacket ret = (CommandPacket)testPacket(cmdpkg);
        assertEquals(ret, cmdpkg);
    }
    
    private Packet testPacket(Packet packet) throws IOException, ClassNotFoundException{
        byte[] result;
        try (ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
             PacketWriter writer = new PacketWriter(outputStream)){
            
            writer.write(packet);
            result = outputStream.toByteArray();
        }
        
        try (ByteArrayInputStream inputStream = new ByteArrayInputStream(result);
            PacketReader reader = new PacketReader(inputStream)){
            
            return reader.read();
        }
    }
}
