/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lib.rexec;

/**
 *
 * @author thomas
 */
public class MathResource {
    
    @RexecMethod
    public int add(Integer a, Integer b){
        return a + b;
    }
    
    @RexecMethod
    public int sub(Integer a, Integer b){
        return a - b;
    }
    
    @RexecMethod
    public void throwEx() throws Exception{
        throw new Exception("Exception test");
    }
    
    public double pi(){
        return Math.PI;
    }
}
