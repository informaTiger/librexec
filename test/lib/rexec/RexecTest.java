/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lib.rexec;

import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import lib.rexec.entity.CommandPacket;
import lib.rexec.entity.DataPacket;
import lib.rexec.entity.Packet;
import lib.rexec.entity.PacketType;
import lib.rexec.net.Client;
import lib.rexec.net.Receiver;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Thomas
 */
public class RexecTest implements Receiver {
    
    private RexecProvider provider;
    
    private Client client;
    
    private Thread srv;
    
    private Thread clt;
    
    public RexecTest() {
    }
    
    @Before
    public void setUp() throws IOException {
        provider = new RexecProvider();
        
        RexecApplication app = new RexecApplication("Math");        
        app.register(MathResource.class);
        
        provider.register(app);
        
        srv = new Thread(provider, "rexec-provider");
        srv.start();
        
        client = new Client(InetAddress.getLoopbackAddress());
        client.addListener(this);
        
        clt = new Thread(client, "rexec-client");
        clt.start();
    }
    
    @After
    public void tearDown() {
        if (srv != null){
            srv.interrupt();
        }
        
        if (clt != null){
            clt.interrupt();
        }
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}

    @Test
    public synchronized void performTest() throws IOException, InterruptedException {
        CommandPacket cmdpkg = new CommandPacket.Builder()
            .entity("Math::lib.rexec.MathResource::add")
            .params(3, 5)
            .build();
        
        client.send(cmdpkg);
        wait();
    }
    
    @Override
    public synchronized void receive(Packet packet, Socket sender) {
        if (packet.getType() == PacketType.COMMAND){
            fail("Invalid packet type: COMMAND");
        }
        DataPacket pkt = (DataPacket)packet;
        
        notify();
        
        assertFalse(pkt.getData() instanceof RemoteException);
        assertEquals(pkt.getData(), 8);
    }

    @Override
    public synchronized void error(Exception ex) {
        fail(ex.getMessage());
        
        notify();
    }
}
